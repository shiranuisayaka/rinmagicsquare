/*
ブロック崩しの各オブジェクトの基本的な要素を集めたクラスです。
継承して使ってください
*/
public abstract class BreakOutObject
{
    private float X_Coordinate = 0;//X座標
    private float Y_Coordinate = 0;//Y座標
    private int X_PPF = 0;
    private int Y_PPF = 0;
    private int Width = 0;//横幅
    private int Height = 0;//縦幅
    //コンストラクターです。各プロパティの要素を記憶しています
    public BreakOutObject (int x,int y,int width,int height) 
    {
        this.X_Coordinate = x;
        this.Y_Coordinate = y;
        this.Width = width;
        this.Height = height;
    }
    public BreakOutObject (float x,float y,int width,int height) 
    {
        this.X_Coordinate = x;
        this.Y_Coordinate = y;
        this.Width = width;
        this.Height = height;
    }
    //基本的にGetterとSetterだけなので説明は省略
    public float GetX_Coordinate()
    {
        return this.X_Coordinate;
    }
    public float GetY_Coordinate()
    {
        return this.Y_Coordinate;
    }
    public int GetWidth()
    {
        return this.Width;
    }
    public int GetHeight()
    {
        return this.Height;
    }
    public void SetX_Coordinate() 
    {
        this.X_Coordinate += this.X_PPF;
    }
    public void SetX_Coordinate(float x)
    {
        this.X_Coordinate = x;
    }
    public void SetX_Coordinate(int x)
    {
        this.X_Coordinate = x;
    }   
    public void SetY_Coordinate() 
    {
        this.Y_Coordinate += this.Y_PPF;
    }
    public void SetY_Coordinate(float y) 
    {
        this.Y_Coordinate = y;
    }
    public void SetY_Coordinate(int y)
    {
        this.Y_Coordinate = y;
    }
}

//ブロック崩しの各オブジェクトの動作を定義したインターフェースです
//あんまり意味無い
interface IBreakOut
{
    void Show();
}