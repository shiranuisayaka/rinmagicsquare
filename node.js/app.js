var CoordinateData =
    {
        "Method_Status" : 25,
        "Left_Bar" : 260.0,
        "Right_Bar" :260.0,
        "Up_Bar" : 260.0,
        "Down_Bar" : 260.0
    };
var OccupyingBarData = 
    {
        "Method_Status" : 23,
        "BarData" : [0,0,0,0]
    };
/*
"Method_Status":23,
"ClientNumber:1,
"BarNumber":
*/
/*
"Method_Status":25,
"Coordinate_Potision" : "Left_Bar",
"Value" : 1 / 2

        "Coordinates" :
            {"Left_Bar" : 1 / 2,
            "Right_Bar" : 1,
            "Up_Bar" :  1,
            "Down_Bar" :  1}
*/
var WebSocketServer = require('ws').Server;
var wss = new WebSocketServer({ port: 50121 });

console.log("WS-SERVER RUNNING ON PORT 50121");

//Websocket接続を保存しておく
var connections = [];
 
//接続時
wss.on('connection', function (ws) {
    //配列にWebSocket接続を保存
    connections.push(ws);
    //切断時
    ws.on('close', function () {
        connections = connections.filter(function (conn, i) {
            return (conn === ws) ? false : true;
        });
    });
    //メッセージ送信時
    ws.on('message', function (message) {
    try
    {
        var ReceivedJsonData = JSON.parse(message);
        if(ReceivedJsonData["Method_Status"] == 25)
        {
            CoordinateData[ReceivedJsonData["Coordinate_Potision"]] = ReceivedJsonData["Value"];
        }
        else if(ReceivedJsonData["Method_Status"] == 23)
        {
            OccupyingBarData["BarData"][ReceivedJsonData["BarNumber"]] = ReceivedJsonData["ClientNumber"];
        }
        else if(ReceivedJsonData["Method_Status"] == 26)
        {
            CoordinateData["Left_Bar"] = 260.0;
            CoordinateData["Right_Bar"] = 260.0;
            CoordinateData["Up_Bar"] = 260.0;
            CoordinateData["Down_Bar"] = 260.0;
            broadcast("{\"Method_Status\":26}");
        }
        else
        {
            broadcast(JSON.stringify(ReceivedJsonData));
        }
    }
    catch(e)
    {
        broadcast(message);
    }
});
});
 
 setInterval(() => broadcast(JSON.stringify(CoordinateData)), 200);
 setInterval(() => broadcast(JSON.stringify(OccupyingBarData)), 500);
 setInterval(() => CheckALLReady(),1000);
 setInterval(() => function(){if(connections.length == 0){OccupyingBarData["BarData"].map(function(x){return 0});}},10000)
//ブロードキャストを行う
function broadcast(message) {
    connections.forEach(function (con, i) {
        try
        {
        con.send(message);
        }
        catch(e)
        {

        }
    });
};
function CheckALLReady()
{
    for(var i = 0;i < 4;i++)
    {
        if(OccupyingBarData["BarData"][i] == 0)
        {
            return;
        }
    }
    broadcast(JSON.stringify(OccupyingBarData));
    for(var i = 0;i < 4;i++)
    {
        OccupyingBarData["BarData"][i] = 0
    }
    broadcast("{\"Method_Status\":24}");
    return;
};