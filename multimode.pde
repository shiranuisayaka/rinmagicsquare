//マルチプレイヤーモード用クラス
//基本的にはシングルプレイモードと同じ
//マルチプレイヤーモードに関しては正式版では無いので、あまりコメント書いてないえす
public class MultiMode 
{
    private BlockControler blocks;
    private BarControler bars;
    private BallControler balls;

    private ControlStatus keyboardstatus;
    private StringList LoginUsers;//セッションに入ってるユーザーの名前
    private IntList UsersReady;//準備完了かどうかを記憶（現在の所使ってない）

    private int passedframecount = 0;

    private int BarNumber = 0;

    private String GUID = "";

    public MultiMode()
    {
        blocks = new BlockControler();
        bars = new BarControler();
        balls = new BallControler();

        keyboardstatus = new ControlStatus();
        
        LoginUsers = new StringList();
        UsersReady = new IntList();

    }
    //名前入力画面
    //引数はキーボードに入力した文字
    public void NewSession(String name)
    {
        fill(255, 255, 255);
        textSize(34);
        textAlign(CENTER);

        text("ENTER YOUR NAME \n(Only Alphanumeric)", width * 0.5, height * 0.2);
        //if(name == null)
        {
            name += "|";
        }
        fill(255, 255, 255);
        rect(width * 0.25, height * 0.5, width * 0.5, height * 0.1);
        fill(0, 0, 0);
        stroke(0, 0, 0);
        text(name, width * 0.5, height * 0.57);

        stroke(0, 102, 165);
		if(mouseX > width * 0.25 && mouseX < width * 0.75
        && mouseY > height * 0.7 && mouseY < height * 0.8)
        {
			fill(0, 102, 165);
        }
		else
		{
			fill(0, 0, 0);
		}
		rect(width * 0.25, height * 0.7, width * 0.5, height * 0.1);
        fill(255, 255, 255);
        stroke(0, 0, 0);
        text("CONNECT", width * 0.5, height * 0.77);
    }
    public void Connecting()
    {
        textAlign(CENTER);
        textSize(40);
        fill(255, 255, 255);
        text("CONNECTING", width * 0.5, height * 0.5);
    }
    public void Lobby()
    {
        textAlign(CENTER);
        textSize(40);
        fill(255, 255, 255);
        for(int i = 0; i < LoginUsers.size(); i++)
        {
            text(LoginUsers.get(i), width * 0.5, height * 0.2 + i * 0.1);
        }

        stroke(0, 102, 165);
		if(mouseX > width * 0.25 && mouseX < width * 0.75
        && mouseY > height * 0.7 && mouseY < height * 0.8)
        {
			fill(0, 102, 165);
        }
		else
		{
			fill(0, 0, 0);
		}
		rect(width * 0.25, height * 0.7, width * 0.5, height * 0.1);
        fill(255, 255, 255);
        stroke(0, 0, 0);
        text("READY", width * 0.5, height * 0.77);
    }
    public void InsertUserInfo(String UserName)
    {
        LoginUsers.append(UserName);
        UsersReady.append(0);
    }
    public void UpdateUserReadyInfo(String UserName)
    {
        for(int i = 0; i < LoginUsers.size() ; i++)
        {
            if(LoginUsers.get(i) == UserName)
            {
                UsersReady.set(i,1);
            }
        }
    }
    public boolean CountDown()
    {
        blocks.ShowBlocks();
		bars.ShowBar();
		balls.ShowBall();
        passedframecount++;
        textAlign(CENTER);
        textSize(40);
        fill(255, 255, 255);
        if(passedframecount < 60)
        {
            text("3", width * 0.5, height * 0.5);
        }
        if(passedframecount > 60 && passedframecount <= 120)
        {
            text("2", width * 0.5, height * 0.5);
        }
        if(passedframecount > 120 && passedframecount <= 180)
        {
            text("1", width * 0.5, height * 0.5);
        }
        if(passedframecount > 180)
        {
            passedframecount = 0;
            return true;
        }
        else
        {
            return false;    
        }
    }
    public boolean Update(ControlStatus keyboardstatus, boolean IsKeyboardControl)
    {
        if(!IsKeyboardControl)
        {
            bars.OnMouseMove(this.BarNumber);
        }
        else
        {
            if(keyboardstatus.IsPressing_Up && (this.BarNumber == 2 || this.BarNumber == 3)){bars.MoveToUp(this.BarNumber);}
            if(keyboardstatus.IsPressing_Down && (this.BarNumber == 2 || this.BarNumber == 3)){bars.MoveToDown(this.BarNumber);}
            if(keyboardstatus.IsPressing_Left && (this.BarNumber == 0 || this.BarNumber == 1)){bars.MoveToLeft(this.BarNumber);}
            if(keyboardstatus.IsPressing_Right && (this.BarNumber == 0 || this.BarNumber == 1)){bars.MoveToRight(this.BarNumber);}
        }
        blocks.ShowBlocks();
		blocks.UpdateAngle();
        bars.ShowBar();
        boolean isoutofcampus = balls.OnUpdatePosition();
        if(isoutofcampus)
        {
            return true;
        }
        balls.ShowBall();

        CheckCollision.CheckCollision_and_InvertBall(blocks,bars,balls);
        return false;
    }
    public void UpdateBarPotision(float bar1, float bar2, float bar3, float bar4)
    {
        this.bars.SetBarsPotision(bar1, bar2, bar3, bar4);
    }
    public String AssembleCoordinateData()
    {
        if(this.BarNumber < 2)
        {
            if(bars.GetX_Coordinate(this.BarNumber) >= 0)
            {
                return "BAR-" + this.BarNumber + "-" + bars.GetX_Coordinate(this.BarNumber);
            }
            else
            {
                return "BAR-" + this.BarNumber + "-0";
            }
        }
        else
        {
            if(bars.GetY_Coordinate(this.BarNumber) >= 0)
            {
                return "BAR-" + this.BarNumber + "-" + bars.GetY_Coordinate(this.BarNumber);
            }
            else
            {
                return "BAR-" + this.BarNumber + "-0";
            }
        }
    }
    public void GameOver()
    {
        blocks.ShowBlocks();
        bars.ShowBar();

        fill(0, 102, 165);
        textAlign(CENTER);
        textSize(40);
        text("GAME OVER", width * 0.5, height * 0.08);
        if(frameCount % 120 > 60)
        {
            text("Click to Menu", width * 0.5, height * 0.95);
        }
    }
    public void SetGUID(String guid)
    {
        this.GUID = guid;
    }
    public String GetGUID()
    {
        return this.GUID;
    }
    public void SetBarID(int barid)
    {
        this.BarNumber = barid;
    }
}
