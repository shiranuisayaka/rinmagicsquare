//シングルプレイモードのクラスです
public class SingleMode 
{
    protected BlockControler blocks;//ブロック
    protected BarControler bars;//バー
    protected BallControler balls;//ボール

    protected int passedframecount = 0;//カウントダウン用

    public SingleMode()
    {
        blocks = new BlockControler();
        bars = new BarControler();
        balls = new BallControler();

        keyboardstatus = new ControlStatus();
    }
	public void Setting() //ボールの初期位置を決める
	{
		blocks.ShowBlocks();
		bars.ShowBar();
		balls.ShowBall();
        balls.SetX_Coordinate(mouseX);
        balls.SetY_Coordinate(mouseY);
        textAlign(CENTER);
        textSize(28);
        if(frameCount % 120 > 60)
        {
            text("CHOOSE INITIAL BALL POTISION AND CLICK!", width * 0.5, height * 0.1);
        }
	}
    public boolean CountDown()//カウントダウン
    {
        blocks.ShowBlocks();
		bars.ShowBar();
		balls.ShowBall();
        passedframecount++;
        textAlign(CENTER);
        textSize(40);
        fill(255, 255, 255);
        if(passedframecount < 60)
        {
            text("3", width * 0.5, height * 0.5);
        }
        if(passedframecount > 60 && passedframecount <= 120)
        {
            text("2", width * 0.5, height * 0.5);
        }
        if(passedframecount > 120 && passedframecount <= 180)
        {
            text("1", width * 0.5, height * 0.5);
        }
        if(passedframecount > 180)//180フレーム=3秒（60fps*3）経ったら開始
        {
            passedframecount = 0;
            return true;
        }
        else
        {
            return false;    
        }
        
    }
    public boolean Update(ControlStatus keyboardstatus, boolean IsKeyboardControl)//画面更新
    {
        if(!IsKeyboardControl)//マウス入力モード
        {
            //bars.OnMouseMove();
            SetBarsPotision_Auto();
        }
        else//キーボード入力モード
        {
            if(keyboardstatus.IsPressing_Up){bars.MoveToUp();}
            if(keyboardstatus.IsPressing_Down){bars.MoveToDown();}
            if(keyboardstatus.IsPressing_Left){bars.MoveToLeft();}
            if(keyboardstatus.IsPressing_Right){bars.MoveToRight();}
        }

        blocks.ShowBlocks();//表示
		blocks.UpdateAngle();//回転させる
        bars.ShowBar();//表示
        boolean isoutofcampus = balls.OnUpdatePosition();
        if(isoutofcampus)
        {
            return true;
        }
        balls.ShowBall();

        CheckCollision.CheckCollision_and_InvertBall(blocks,bars,balls);
        return false;
    }
    public void GameOver()//ゲームオーバー画面
    {
        blocks.ShowBlocks();
        bars.ShowBar();

        fill(0, 102, 165);
        textAlign(CENTER);
        textSize(40);
        text("GAME OVER", width * 0.5, height * 0.08);
        if(frameCount % 120 > 60)
        {
            text("Click to Menu", width * 0.5, height * 0.95);
        }
    }
   public void SetBarsPotision(float bar1, float bar2, float bar3, float bar4)
   {
       bars.SetBarsPotision(bar1,bar2,bar3,bar4);
   }
   public void SetBarsPotision_Relative(float bar1, float bar2, float bar3, float bar4)
   {
       bars.SetBarsPotision_Relative(bar1,bar2,bar3,bar4);
   }
   public void SetBarsPotision_Auto()
   {
       bars.SetBarsPotision(balls.GetX_Coordinate()- 20,balls.GetX_Coordinate() - 20,balls.GetY_Coordinate() - 20,balls.GetY_Coordinate() - 20);
   }
}