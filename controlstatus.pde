//主にキーボード入力の際にキーが押されているかどうかを記憶する為に使います
public class ControlStatus
{
    public boolean IsPressing_Right = false;
    public boolean IsPressing_Left = false;
    public boolean IsPressing_Up = false;
    public boolean IsPressing_Down = false;
}
