﻿$(function () {
    var ws = new WebSocket("ws://150.95.175.31:50121/");
    $("#up_button").on('touchstart pointerdown', function () {
        ws.send("UP/DOWN");
    });
    $("#up_button").on('touchend pointerup', function () {
        ws.send("UP/UP");
    });
    $("#down_button").on('touchstart pointerdown', function () {
        ws.send("DOWN/DOWN");
    });
    $("#down_button").on('touchend pointerup', function () {
        ws.send("DOWN/UP");
    });
    $("#left_button").on('touchstart pointerdown', function () {
        ws.send("LEFT/DOWN");
    });
    $("#left_button").on('touchend pointerup', function () {
        ws.send("LEFT/UP");
    });
    $("#right_button").on('touchstart pointerdown', function () {
        ws.send("RIGHT/DOWN");
    });
    $("#right_button").on('touchend pointerup', function () {
        ws.send("RIGHT/UP")
    });
});