//反射板です。BreakOutObjectを継承してほぼそのまま使っています
public class BarModel extends BreakOutObject implements IBreakOut
{
    private boolean MoveLock = false;
    //コンストラクターです。BreakOutObjectのコンストラクターを呼び出しているだけです
    public BarModel (int x,int y,int width,int height)  
    {
        super(x,y,width,height);
    }
    public void Show()
    {
        fill(0,0,255);
        rect(super.X_Coordinate, super.Y_Coordinate, super.Width, super.Height);
    }
    public void Show_MultiMode(int status)
    {
        if(status == 0)
        {
            fill(255,0,0);
        }
        if(status == 1)
        {
            fill(0,255,0);
        }
        if(status == 2)
        {
            fill(0,0,255);
        }
        rect(super.X_Coordinate, super.Y_Coordinate, super.Width, super.Height);
    }
    public void SetLock(){this.MoveLock = true;}
    public void SetUnLock(){this.MoveLock = false;}
    public boolean GetMoveLock(){return this.MoveLock;}
}

//反射板の集合を扱うクラス
public class BarControler
{
    private BarModel[] bars = new BarModel[4];//反射板は4つ
    public BarControler()
    {
        this.bars[0] = new BarModel(width / 2 - 40,0,80,10);//UP
        this.bars[1] = new BarModel(width / 2 - 40,height - 11,80,10);//DOWN
        this.bars[2] = new BarModel(0,height / 2 - 40,10,80);//LEFT
        this.bars[3] = new BarModel(width - 10 - 1,height / 2 - 40,10,80);//RIGHT
    }
    //↓Getterの皆さん
    public float GetX_Coordinate(int number){return this.bars[number].GetX_Coordinate();}
    public float GetY_Coordinate(int number){return this.bars[number].GetY_Coordinate();}
    public float GetHeight(int number){return this.bars[number].GetHeight();}
    public float GetWidth(int number){return this.bars[number].GetWidth();}
    //4つの反射板を表示させる
    public void ShowBar()
    {
        for(int i = 0; i < 4; i++)
        {
            this.bars[i].Show();
        }
    }
    public void ShowBar(int[] barstatus)
    {
        for(int i = 0; i < 4; i++)
        {
            this.bars[i].Show_MultiMode(barstatus[i]);
        }
    }
    //マウス入力時使う
    public void OnMouseMove()
    {
        for(int i = 0; i < bars.length ; i++)
        {
            if(!this.bars[i].GetMoveLock())//デバッグ用に移動をロックさせる事が出来る
            {
                if(i == 0 || i == 1)
                {
                    this.bars[i].SetX_Coordinate(mouseX - bars[i].GetWidth() / 2);
                }
                else
                {
                    this.bars[i].SetY_Coordinate(mouseY - bars[i].GetHeight() / 2);
                }
            }
        }
    }
    //マルチプレイヤー用、特定のバーだけ動かせる（オーバーロード）
    public void OnMouseMove(int BarNumber)
    {
        if(BarNumber == 0 || BarNumber == 1)
        {
            this.bars[BarNumber].SetX_Coordinate(mouseX - bars[BarNumber].GetWidth() / 2);
        }
        else
        {
            this.bars[BarNumber].SetY_Coordinate(mouseY - bars[BarNumber].GetHeight() / 2);
        }
    }
    //UDLR
    //衝突判定。仕組みは単純で、ボールの上下左右がバーに入ってるかどうかを判定しているだけ
    public int CheckCollision(float ball_x,float ball_y,int radius)
    {
        float ball_right_X = ball_x + radius;
        float ball_left_X = ball_x - radius;
        float ball_up_X = ball_x;
        float ball_down_X = ball_x;

        float ball_right_Y = ball_y;
        float ball_left_Y = ball_y;
        float ball_up_Y = ball_y - radius;
        float ball_down_Y = ball_y + radius;

        for(int i = 0; i < this.bars.length ; i++)
        {
            float bar_up_Y = this.bars[i].GetY_Coordinate();
            float bar_down_Y = this.bars[i].GetY_Coordinate() + this.bars[i].GetHeight();
            float bar_left_X = this.bars[i].GetX_Coordinate();
            float bar_right_X = this.bars[i].GetX_Coordinate() + this.bars[i].GetWidth();
            
            switch (i) 
            {
                case 0:
                    if(CheckCollision.CheckBetWeen(bar_up_Y,bar_down_Y,ball_up_Y) && CheckCollision.CheckBetWeen(bar_left_X,bar_right_X,ball_up_X))
                    {
                        return 0;//上のバーに当たった
                    }
                break;

                case 1:
                    if(CheckCollision.CheckBetWeen(bar_up_Y,bar_down_Y,ball_down_Y) && CheckCollision.CheckBetWeen(bar_left_X,bar_right_X,ball_down_X))
                    {
                        return 1;//下のバーに当たった
                    }
                break;

                case 2:
                    if(CheckCollision.CheckBetWeen(bar_up_Y,bar_down_Y,ball_left_Y) && CheckCollision.CheckBetWeen(bar_left_X,bar_right_X,ball_left_X))
                    {
                        return 2;//左のバーに当たった
                    }
                break;
                case 3:
                    if(CheckCollision.CheckBetWeen(bar_up_Y,bar_down_Y,ball_right_Y) && CheckCollision.CheckBetWeen(bar_left_X,bar_right_X,ball_right_X))
                    {
                        return 3;//右のバーに当たった
                    }
                break;
            }
        }
         return 21;//何も無かったら21
    }
    final int PPF_KEYBOARD = 5;
    //バーを上に移動させる
    public void MoveToUp()
    {
        if(this.bars[2].GetY_Coordinate() - PPF_KEYBOARD >= 0 && this.bars[2].GetY_Coordinate() + this.bars[2].GetHeight() - PPF_KEYBOARD <= width)
        {        
            this.bars[2].SetY_Coordinate(this.bars[2].GetY_Coordinate() - PPF_KEYBOARD);
            this.bars[3].SetY_Coordinate(this.bars[3].GetY_Coordinate() - PPF_KEYBOARD);
        }
    }
    //マルチプレイヤー用、特定のバーだけ動かせる（オーバーロード）
    //以下同じなので省略
    public void MoveToUp(int BarNumber)
    {
        if(this.bars[BarNumber].GetY_Coordinate() - PPF_KEYBOARD >= 0 && this.bars[BarNumber].GetY_Coordinate() + this.bars[BarNumber].GetHeight() - PPF_KEYBOARD <= width)
        {        
            this.bars[BarNumber].SetY_Coordinate(this.bars[BarNumber].GetY_Coordinate() - PPF_KEYBOARD);
        }
    }
    public void MoveToDown()
    {
        if(this.bars[2].GetY_Coordinate() + PPF_KEYBOARD >= 0 && this.bars[2].GetY_Coordinate() + this.bars[2].GetHeight() + PPF_KEYBOARD <= width)
        {        
            this.bars[2].SetY_Coordinate(this.bars[2].GetY_Coordinate() + PPF_KEYBOARD);
            this.bars[3].SetY_Coordinate(this.bars[3].GetY_Coordinate() + PPF_KEYBOARD);
        }
    }
    public void MoveToDown(int BarNumber)
    {
        if(this.bars[BarNumber].GetY_Coordinate() + PPF_KEYBOARD >= 0 && this.bars[BarNumber].GetY_Coordinate() + this.bars[BarNumber].GetHeight() + PPF_KEYBOARD <= width)
        {        
            this.bars[BarNumber].SetY_Coordinate(this.bars[BarNumber].GetY_Coordinate() + PPF_KEYBOARD);
        }
    }
    public void MoveToLeft()
    {
        if(this.bars[0].GetX_Coordinate() - PPF_KEYBOARD >= 0 && this.bars[0].GetX_Coordinate() + this.bars[0].GetWidth() - PPF_KEYBOARD <= width)
        {        
            this.bars[0].SetX_Coordinate(this.bars[0].GetX_Coordinate() - PPF_KEYBOARD);
            this.bars[1].SetX_Coordinate(this.bars[1].GetX_Coordinate() - PPF_KEYBOARD);
        }
    }
    public void MoveToLeft(int BarNumber)
    {
        if(this.bars[BarNumber].GetX_Coordinate() - PPF_KEYBOARD >= 0 && this.bars[BarNumber].GetX_Coordinate() + this.bars[BarNumber].GetWidth() - PPF_KEYBOARD <= width)
        {        
            this.bars[BarNumber].SetX_Coordinate(this.bars[BarNumber].GetX_Coordinate() - PPF_KEYBOARD);
        }
    }
    public void MoveToRight()
    {
        if(this.bars[0].GetX_Coordinate() + PPF_KEYBOARD >= 0 && this.bars[0].GetX_Coordinate() + this.bars[0].GetWidth() + PPF_KEYBOARD <= width)
        {
            this.bars[0].SetX_Coordinate(this.bars[0].GetX_Coordinate() + PPF_KEYBOARD);
            this.bars[1].SetX_Coordinate(this.bars[1].GetX_Coordinate() + PPF_KEYBOARD);
        }
    }
    public void MoveToRight(int BarNumber)
    {
        if(this.bars[BarNumber].GetX_Coordinate() + PPF_KEYBOARD >= 0 && this.bars[BarNumber].GetX_Coordinate() + this.bars[BarNumber].GetWidth() + PPF_KEYBOARD <= width)
        {        
            this.bars[BarNumber].SetX_Coordinate(this.bars[BarNumber].GetX_Coordinate() + PPF_KEYBOARD);
        }
    }
    //UDLR
    //マルチプレイヤー用、4つのバーの座標をサーバーと同期させるときに使う
    public void SetBarsPotision(float bar1, float bar2, float bar3, float bar4)
    {
        this.bars[0].SetX_Coordinate(bar1);
        this.bars[1].SetX_Coordinate(bar2);
        this.bars[2].SetY_Coordinate(bar3);
        this.bars[3].SetY_Coordinate(bar4);
    }
    public void SetBarsPotision(int barNumber, float coordinate)
    {
        if(barNumber == 0 || barNumber == 1)
        {
            this.bars[barNumber].SetX_Coordinate(coordinate);
        }
        else
        {
            this.bars[barNumber].SetY_Coordinate(coordinate);
        }
    }
    public void SetBarsPotision_Relative(float bar1, float bar2, float bar3, float bar4)
    {
        this.bars[0].SetX_Coordinate(bar1 * width);
        this.bars[1].SetX_Coordinate(bar2 * width);
        this.bars[2].SetY_Coordinate(bar3 * height);
        this.bars[3].SetY_Coordinate(bar4 * height);
    }
}