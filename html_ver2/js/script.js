var ws;
function setup() 
{
    createCanvas(windowWidth, windowHeight);
    ws = new WebSocket("ws://150.95.175.31:50121/");
}
             
function draw() 
{
    background(255,255,255);
    rect(winMouseX - windowWidth * 0.15,0,windowWidth * 0.3,windowHeight * 0.03);//UP
    rect(winMouseX - windowWidth * 0.15,windowHeight * 0.9,windowWidth * 0.3,windowHeight * 0.03);//DOWN
    rect(0,winMouseY - windowHeight * 0.1,windowWidth * 0.05,windowHeight * 0.2);//LEFT
    rect(windowWidth * 0.9,winMouseY - windowHeight * 0.1,windowWidth * 0.05,windowHeight * 0.2);//RIGHT
    var JsonData = 
    {
        "Message_ID" : 1,
        "Sender_ID" : 1,
        "Sender_Name" : "",
        "Method":"",
        "Coordinates" :
            {"Up_Bar" : winMouseX / windowWidth,
            "Down_Bar" : winMouseX / windowWidth,
            "Left_Bar" :  winMouseY / windowHeight,
            "Right_Bar" :  winMouseY / windowHeight}
    }
    //console.log(JsonData);
    i//f(ws.readyState == 1 && frameCount % 10 == 9)
    {
        ws.send(JSON.stringify(JsonData));
    }
}