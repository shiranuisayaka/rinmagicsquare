//当たり判定関係のメソッド
public static class CheckCollision
{
    static int StopInverseCount = 5;
    static void CheckCollision_and_InvertBall(BlockControler blocks,BarControler bars,BallControler balls)
    {
        if(StopInverseCount != 5)//5フレームの間、当たり判定を無くして多重判定を防ぐ
        {
            StopInverseCount = StopInverseCount != 0 ? StopInverseCount -= 1 : 5;
        }
        //反射板の判定
        int barsCheckResult = bars.CheckCollision(balls.GetX_Coordinate(),balls.GetY_Coordinate(),4);
        //ブロックの判定
        FloatList blocksCheckResult = blocks.CheckCollision(balls.GetX_Coordinate(),balls.GetY_Coordinate(),4);
        int resultCheck = blocksCheckResult.get(0) == 21
                        ? barsCheckResult
                        : int(blocksCheckResult.get(0) + 4);
        if(resultCheck != 21)
        {
            println(resultCheck);
            switch (resultCheck) 
            {
                case 0 : //バーの上→下への反転命令
                    balls.InverseYDirection(2);
                    blocks.ResetInversed();
                break;
                case 1 : //バーの下→上への反転命令
                    balls.InverseYDirection(-2);
                    blocks.ResetInversed();
                break;
                case 2 : //バーの左→右への反転命令
                    balls.InverseXDirection(2);
                    blocks.ResetInversed();
                break;
                case 3 : //バーの右→左への反転命令
                    balls.InverseXDirection(-2);
                    blocks.ResetInversed();
                break;
                case 4 : //ブロックの上→下への反転命令
                    if(blocksCheckResult.get(2) == 0)
                    {
                        balls.InverseYDirection(2);
                    }
                    balls.SetY_Coordinate(blocksCheckResult.get(1));
                    println(blocksCheckResult.get(1));
                break;
                case 5 : //ブロックの下→上への反転命令
                    if(blocksCheckResult.get(2) == 0)
                    {
                        balls.InverseYDirection(-2);
                    }
                    balls.SetY_Coordinate(blocksCheckResult.get(1));
                    println(blocksCheckResult.get(1));
                break;
                case 6 : //ブロックの左→右への反転命令
                    if(blocksCheckResult.get(2) == 0)
                    {
                        balls.InverseXDirection(2);
                    }
                    balls.SetX_Coordinate(blocksCheckResult.get(1));
                    println(blocksCheckResult.get(1));
                break;
                case 7 : //ブロックの右→左への反転命令
                    if(blocksCheckResult.get(2) == 0)
                    {                
                        balls.InverseXDirection(-2);
                    }
                    balls.SetX_Coordinate(blocksCheckResult.get(1));
                    println(blocksCheckResult.get(1));
                break;
                case 8 : //ブロックの左上→左上への反転命令
                    if(blocksCheckResult.get(2) == 0)
                    {
                        balls.InverseYDirection(-2);
                        balls.InverseXDirection(-2);
                    }
                    println("LU");
                    println(blocksCheckResult.get(1));
                break;
                case 9 : //ブロックの左下→左下への反転命令
                    if(blocksCheckResult.get(2) == 0)
                    {                    
                        balls.InverseYDirection(2);
                        balls.InverseXDirection(-2);
                    }
                    println("LD");
                    println(blocksCheckResult.get(1));
                break;
                case 10 : //ブロックの右上→右上への反転命令
                    if(blocksCheckResult.get(2) == 0)
                    {                    
                        balls.InverseYDirection(-2);
                        balls.InverseXDirection(2);
                    }
                    println("RU");
                    println(blocksCheckResult.get(1));
                break;
                case 11 : //ブロックの右下→右下への反転命令
                    if(blocksCheckResult.get(2) == 0)
                    {                    
                        balls.InverseYDirection(2);
                        balls.InverseXDirection(2);
                    }
                    println("RD");
                    println(blocksCheckResult.get(1));
                break;
            }
            StopInverseCount--;
        }
    }
    //AがXとYの間にあればtrue,無ければfalseを返す関数です
    public static boolean CheckBetWeen(float X,float Y,float A)
    {
        return (X <= A && A <= Y) || (X >= A && A >= Y);
    }
}


