//ボールです。BreakOutObjectを継承してほぼそのまま使っています
public class BallModel extends BreakOutObject implements IBreakOut
{
    //コンストラクターです。BreakOutObjectのコンストラクターを呼び出しているだけです
    public BallModel (int x,int y,int width,int height)  
    {
        super(x,y,width,height);
        super.X_PPF = 1;
        super.Y_PPF = 1;
    }
    /*
    public BallModel (int width,int height)  
    {
        super(random(100, 500),random(100, 500),width,height);
        super.X_PPF = 2;
        super.Y_PPF = 0;
    }
    */
    public BallModel (int width,int height)  
    {
        //super(random(100, 500),random(100, 500),width,height);
        super(300,300,width,height);
        int xppf = random(0, 1) == 1 ? 2 : -2;
        int yppf = random(0, 1) == 1 ? 2 : -2;
        super.X_PPF = xppf;
        super.Y_PPF = yppf;
    }
    public void Show()
    {
        fill(255, 255, 255);
        ellipse(super.X_Coordinate, super.Y_Coordinate, super.Width, super.Height);
    }
    public void SetX_PPF()
    {
        super.X_PPF *= -1;
    }
    public void SetX_PPF(int direction)
    {
        //direction = direction > 0 ? direction - 1 : direction + 1;
        super.X_PPF = direction;
    }
    public void SetY_PPF()
    {
        super.Y_PPF *= -1;
    }
    public void SetY_PPF(int direction)
    {
        //direction = direction > 0 ? direction - 1 : direction + 1;
        super.Y_PPF = direction;
    }
}


public class BallControler
{
    private BallModel ball;
    public BallControler()
    {
        this.ball = new BallModel(/*30,300,*/8,8);
    }
    public void ShowBall()
    {
        this.ball.Show();
    }
    public boolean OnUpdatePosition()
    {
        this.ball.SetX_Coordinate();
        this.ball.SetY_Coordinate();
        if(!CheckCollision.CheckBetWeen(0,width,this.ball.GetX_Coordinate())
        || !CheckCollision.CheckBetWeen(0,height,this.ball.GetY_Coordinate()))
        {
            return true;
        }
        else
        {
            return false;    
        }
    }
    public float GetX_Coordinate()
    {
        return this.ball.GetX_Coordinate();
    }
    public float GetY_Coordinate()
    {
        return this.ball.GetY_Coordinate() /*+ 8*/;
    }
    public void InverseXDirection()
    {
        this.ball.SetX_PPF();
    }
    public void InverseXDirection(float x_Coordinate)
    {
        this.ball.SetX_PPF();
        this.ball.SetX_Coordinate(x_Coordinate);
    }
    public void InverseXDirection(int direction)
    {
        this.ball.SetX_PPF(direction);
    }
    public void InverseYDirection()
    {
        this.ball.SetY_PPF();
    }
    public void InverseYDirection(float y_Coordinate)
    {
        this.ball.SetY_PPF();
        this.ball.SetY_Coordinate(y_Coordinate);
    }
    public void InverseYDirection(int direction)
    {
        this.ball.SetY_PPF(direction);
    }
    public void SetX_Coordinate(float x)
    {
        this.ball.SetX_Coordinate(x);
    }
    public void SetY_Coordinate(float y)
    {
        this.ball.SetY_Coordinate(y);
    }
}