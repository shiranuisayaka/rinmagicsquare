//RiNMagicSquare Version 2.0 RC1
//Created By Shiranui Nui @shirasayav5
//Latest Success Build 2017/6/8

import websockets.*;//WebSocket通信ライブラリ

int WindowStatus = 0;//画面遷移管理変数
SingleMode singlemode = null;//シングルプレイモードのクラスのインスタンス
MultiMode_ws multimode = null;//マルチプレイモードのクラスのインスタンス
StartMenu startmenu = new StartMenu();//スタートメニューのクラスのインスタンス

WebsocketClient wsc;

ControlStatus keyboardstatus;

boolean IsKeyboardControl = false;//キーボード入力かどうか
boolean IsSmartPhoneControl = false;//マウス入力かどうか

boolean[] MultiMode_ClientControling = {false,false,false,false};
int[] MultiMode_BarStatus = null;

String JSON_SendString = null;

boolean SendedEndGame = false;

//ENTRY POINT
void setup() 
{
    size(600, 600);
    frameRate(60);
    keyboardstatus = new ControlStatus();
    MultiMode_BarStatus = new int[]{0, 0, 0, 0};//UDLR 0:未占有 1:他人が占有 2:自分が占有
}

void draw() 
{
    InitializeCanpus();//画面の初期化
    if(frameCount % 60 == 0) {
  }
    switch (WindowStatus)//画面遷移の分岐
    {
        case 0://メニュー画面
            startmenu.ShowStartMenu(IsKeyboardControl,IsSmartPhoneControl);
            singlemode = null;
            multimode = null;
            wsc = null;
            SendedEndGame = false;
            break;

            //シングルプレイモードにおける画面推移 11->15->12->13
		case 11://シングルプレイモード/ボールの初期位置設定
			if(singlemode == null){singlemode = new SingleMode();}
            singlemode.Setting();
            break;
        case 12://シングルプレイモード/ゲーム中
        /*
			if(IsSmartPhoneControl)//スマホ操作モードの時の処理
			{
                if(WebSocketMessage != "")
                {
               		String[] ReceivedData = split(WebSocketMessage,"/");
                    switch(ReceivedData[0])
                    {
                        case "RIGHT":
                            keyboardstatus.IsPressing_Right = ReceivedData[1].equals("DOWN");
                            break;
                        case "LEFT":
                            keyboardstatus.IsPressing_Left = ReceivedData[1].equals("DOWN");
                            break;
                        case "DOWN":
                            keyboardstatus.IsPressing_Down = ReceivedData[1].equals("DOWN");
                            break;
                        case "UP":
                            keyboardstatus.IsPressing_Up = ReceivedData[1].equals("DOWN");
                            break;
                    }
                    WebSocketMessage = "";
                }
                */
                /*
	            if(UDPReceivedMessage != "")
				{
               		String[] ReceivedData = split(UDPReceivedMessage,'-');
					if(ReceivedData[0].equals("KEY"))//UDPで受信したデータの解析&キーボード入力確認変数への代入
					{
						keyboardstatus.IsPressing_Right = boolean(ReceivedData[1]);
						keyboardstatus.IsPressing_Left = boolean(ReceivedData[2]);
						keyboardstatus.IsPressing_Up = boolean(ReceivedData[3]);
						keyboardstatus.IsPressing_Down = boolean(ReceivedData[4]);
					}
				}
			}
            */
            boolean isoutofcampus = singlemode.Update(keyboardstatus,IsKeyboardControl);//画面描画
            if(isoutofcampus)//画面外にボールが出たらゲームオーバー画面へ
            {
                WindowStatus = 13;
            }
    	    break;
        case 13://シングルプレイモード/ゲームオーバー画面
            singlemode.GameOver();
            break;
        case 15://シングルプレイモード/カウントダウン画面
			if(singlemode == null){singlemode = new SingleMode();}
            if(singlemode.CountDown())
            {
                WindowStatus = 12;
            }
            break;
        case 22://マルチプレイヤーモード/接続画面
			if(multimode == null){multimode = new MultiMode_ws();}
            multimode.Connecting();
            wsc = new WebsocketClient(this, "ws://150.95.175.31:50121/");
            //if(Occupying_JSON == null){Occupying_JSON = new JSONObject();}
            break;
        case 23://マルチプレイヤーモード/準備画面
            multimode.Setting(MultiMode_BarStatus);
            break;
        case 24:
            break;
        case 25://マルチプレイヤーモード/プレイ画面
            isoutofcampus = multimode.Update(keyboardstatus,IsKeyboardControl,MultiMode_ClientControling,MultiMode_BarStatus);//画面描画
            if(isoutofcampus)//画面外にボールが出たらゲームオーバー画面へ
            {
                WindowStatus = 26;
                break;
            }
            if(frameCount % 10 == 2)
            {
                if(MultiMode_ClientControling[0])
                {
                    JSON_SendString = String.format("{\"Method_Status\":25,\"Coordinate_Potision\":\"Up_Bar\",\"Value\":%f}",multimode.GetCoordinate(0));
                    wsc.sendMessage(JSON_SendString);
                }
                if(MultiMode_ClientControling[1])
                {
                    JSON_SendString = String.format("{\"Method_Status\":25,\"Coordinate_Potision\":\"Down_Bar\",\"Value\":%f}",multimode.GetCoordinate(1));
                    wsc.sendMessage(JSON_SendString);
                }
                if(MultiMode_ClientControling[2])
                {
                    JSON_SendString = String.format("{\"Method_Status\":25,\"Coordinate_Potision\":\"Left_Bar\",\"Value\":%f}",multimode.GetCoordinate(2));
                    wsc.sendMessage(JSON_SendString);
                }
                if(MultiMode_ClientControling[3])
                {
                    JSON_SendString = String.format("{\"Method_Status\":25,\"Coordinate_Potision\":\"Right_Bar\",\"Value\":%f}",multimode.GetCoordinate(3));
                    wsc.sendMessage(JSON_SendString );
                }
            }
            break;
        case 26://マルチプレイヤーモード/ゲームオーバー画面
            multimode.GameOver(MultiMode_BarStatus);
            if(!SendedEndGame)
            {
                wsc.sendMessage("{\"Method_Status\":26}");
                SendedEndGame = true;
            }
            break;
        case 29://マルチプレイヤーモード/カウントダウン画面
            if(multimode.CountDown(MultiMode_BarStatus))
            {
                WindowStatus = 25;
            }
            break;
        case 31://シングルプレイヤーモード/スマートフォン操作画面
			textAlign(CENTER);
			textSize(40);
			fill(255, 255, 255);
			text("CONNECTING", width * 0.5, height * 0.5);
            wsc = new WebsocketClient(this, "ws://150.95.175.31:50121/");
            WindowStatus = 11;
            /*
	        UDPClient.send("NEWCONNECT", "localhost", 50126);//UDPでスマートフォン操作の為の接続要求を送信
        	println("SEND:" + "NEWCONNECT");
			
            if(UDPReceivedMessage != "")//ACKを受信したらシングルプレイモード/準備画面へ
            {
				WindowStatus = 11;
            }
            */
            break;
    }
}

void InitializeCanpus()//画面初期化処理
{
    background(0, 0, 0);//黒
    fill(0, 0, 0);
}

void mousePressed() 
{
    switch(WindowStatus)
    {
        case 0://メニュー画面
            if(mouseX > width * 0.25 && mouseX < width * 0.75
            && mouseY > height * 0.5 && mouseY < height * 0.6)
            {
                if(!IsSmartPhoneControl)
                {
                    WindowStatus = 11;//スマホ操作モードで無いとき、シングルプレイ準備モードへ
                }
                else
                {
                    WindowStatus = 31;//スマホ操作モード接続画面へ
                }
            }
            else if(mouseX > width * 0.25 && mouseX < width * 0.75
            && mouseY > height * 0.7 && mouseY < height * 0.8)
            {
                WindowStatus = 22;//マルチ画面へ
            }
            else if(mouseX > width * 0.03 && mouseX < width * 0.06
            && mouseY > height * 0.85 && mouseY < height * 0.88)//マウス操作モード切替
            {
                IsKeyboardControl = false;
                IsSmartPhoneControl = false;
            }
            else if(mouseX > width * 0.03 && mouseX < width * 0.06
            && mouseY > height * 0.89 && mouseY < height * 0.92)//キーボード入力モード切替
            {
                IsKeyboardControl = true;
                IsSmartPhoneControl = false;
            }
            else if(mouseX > width * 0.03 && mouseX < width * 0.06
            && mouseY > height * 0.93 && mouseY < height * 0.96)//スマホ操作モード切替
            {
                IsSmartPhoneControl = true;
				IsKeyboardControl = true;
            }
            return;

        case 11://シングルプレイモード初期位置設定画面
            WindowStatus = 15;
            return;
        case 13://シングルプレイモードゲームオーバー画面
            WindowStatus = 0;
            return;

        case 21://マルチプレイヤーモード名前入力画面
            return;
        case 23://マルチプレイヤーモード準備画面
            if(mouseX > width / 2 - 40 && mouseX < width / 2 + 40
            && mouseY > 0 && mouseY < 10 && MultiMode_BarStatus[0] != 1)
            {
                MultiMode_ClientControling[0] = !MultiMode_ClientControling[0];
                MultiMode_BarStatus[0] = MultiMode_BarStatus[0] == 0 ? 2 : MultiMode_BarStatus[0] == 2 ? 0 : 1;
                JSON_SendString =
                        String.format("{\"Method_Status\":23,\"ClientNumber\":%d,\"BarNumber\":%d}",MultiMode_BarStatus[0] == 2 ? multimode.RandomNumber : 0,0);
                wsc.sendMessage(JSON_SendString);
            }
            if(mouseX > width / 2 - 40 && mouseX < width / 2 + 40
            && mouseY > height - 11 && mouseY < height && MultiMode_BarStatus[1] != 1)
            {
                MultiMode_ClientControling[1] = !MultiMode_ClientControling[1];
                MultiMode_BarStatus[1] = MultiMode_BarStatus[1] == 0 ? 2 : MultiMode_BarStatus[1] == 2 ? 0 : 1;
                JSON_SendString =
                        String.format("{\"Method_Status\":23,\"ClientNumber\":%d,\"BarNumber\":%d}",MultiMode_BarStatus[1] == 2 ? multimode.RandomNumber : 0,1);
                wsc.sendMessage(JSON_SendString);
            }
            if(mouseX > 0 && mouseX < 10
            && mouseY > height / 2 - 40 && mouseY < height / 2 + 80 && MultiMode_BarStatus[2] != 1)
            {
                MultiMode_ClientControling[2] = !MultiMode_ClientControling[2];
                MultiMode_BarStatus[2] = MultiMode_BarStatus[2] == 0 ? 2 : MultiMode_BarStatus[2] == 2 ? 0 : 1;
                JSON_SendString =
                        String.format("{\"Method_Status\":23,\"ClientNumber\":%d,\"BarNumber\":%d}",MultiMode_BarStatus[2] == 2 ? multimode.RandomNumber : 0,2);
                wsc.sendMessage(JSON_SendString);
            }
            if(mouseX > width - 11 && mouseX < width
            && mouseY > height / 2 - 40 && mouseY < height / 2 + 80 && MultiMode_BarStatus[3] != 1)
            {
                MultiMode_ClientControling[3] = !MultiMode_ClientControling[3];
                MultiMode_BarStatus[3] = MultiMode_BarStatus[3] == 0 ? 2 : MultiMode_BarStatus[3] == 2 ? 0 : 1;
                JSON_SendString =
                        String.format("{\"Method_Status\":23,\"ClientNumber\":%d,\"BarNumber\":%d}",MultiMode_BarStatus[3] == 2 ? multimode.RandomNumber : 0,3);
                wsc.sendMessage(JSON_SendString);
            }
            return;
        case 24://デバッグ用ダミー
            return;
        case 26://マルチプレイヤーモードゲームオーバー画面
            WindowStatus = 0;
            return;
    }
}

 void keyPressed()
{
    //if(WindowStatus != 21)
    {
        switch (keyCode) //キーボード入力時、押してるか離れてるかを確認する
        {
            case RIGHT:
                keyboardstatus.IsPressing_Right = true;
                break;
            case LEFT:
                keyboardstatus.IsPressing_Left = true;
                break;
            case UP:
                keyboardstatus.IsPressing_Up = true;
                break;
            case DOWN:
                keyboardstatus.IsPressing_Down = true;
                break;
        }
    }
}
void keyReleased()
{
    switch (keyCode)  //キーボード入力時、押してるか離れてるかを確認する
    {
        case RIGHT:
            keyboardstatus.IsPressing_Right = false;
            break;
        case LEFT:
            keyboardstatus.IsPressing_Left = false;
            break;
        case UP:
            keyboardstatus.IsPressing_Up = false;
            break;
        case DOWN:
            keyboardstatus.IsPressing_Down = false;
            break;
    }
}
void webSocketEvent(String msg)
{
    /*
    if(WindowStatus == 12 && IsSmartPhoneControl)
    {
        println(Json.getJSONObject("Coordinates").getFloat("Left_Bar"));

        singlemode.SetBarsPotision_Relative(
        Json.getJSONObject("Coordinates").getFloat("Up_Bar"),
        Json.getJSONObject("Coordinates").getFloat("Down_Bar"),
        Json.getJSONObject("Coordinates").getFloat("Left_Bar"),
        Json.getJSONObject("Coordinates").getFloat("Right_Bar"));
    }
    */
    if(WindowStatus == 22)
    {
        WindowStatus = 23;
    }
    if(WindowStatus == 23)
    {
        JSONObject Json = parseJSONObject(msg);
        if(Json.getInt("Method_Status") == 23)
        {
            JSONArray jsonarraydata = Json.getJSONArray("BarData");
            int[] intarraydata = jsonarraydata.getIntArray();
            for(int i = 0;i < 4; i++)
            {
                if(intarraydata[i] != multimode.RandomNumber && !MultiMode_ClientControling[i])
                {
                    MultiMode_BarStatus[i] = 1;
                }
                if (intarraydata[i] == 0 && !MultiMode_ClientControling[i]) 
                {
                    MultiMode_BarStatus[i] = 0;
                }
            }
        }
        else if(Json.getInt("Method_Status") == 24)
        {
            WindowStatus = 29;
        }
    }
    if(WindowStatus == 25)
    {
        JSONObject Json = parseJSONObject(msg);
        if(Json.getInt("Method_Status") == 25)
        {
            if(!MultiMode_ClientControling[0])
            {
                multimode.SetBarsPotision(0,Json.getFloat("Up_Bar"));
            }
            if(!MultiMode_ClientControling[1])
            {
                multimode.SetBarsPotision(1,Json.getFloat("Down_Bar"));
            }
            if(!MultiMode_ClientControling[2])
            {
                multimode.SetBarsPotision(2,Json.getFloat("Left_Bar"));
            }
            if(!MultiMode_ClientControling[3])
            {
                multimode.SetBarsPotision(3,Json.getFloat("Right_Bar"));
            }
            /*
            multimode.SetBarsPotision(
            !MultiMode_ClientControling[0] ? Json.getFloat("Up_Bar") : mouseX,
            !MultiMode_ClientControling[1] ? Json.getFloat("Down_Bar") : mouseX,
            !MultiMode_ClientControling[2] ? Json.getFloat("Left_Bar") : mouseY,
            !MultiMode_ClientControling[3] ? Json.getFloat("Right_Bar") : mouseY
            );
            */
        }
        else if(Json.getInt("Method_Status") == 26)
        {
            WindowStatus = 26;
        }
    }
}