﻿【最優先】
・背景を地球と宇宙っぽく
・敵キャラはブロック型UFOっぽく
・敵キャラ動かす（上から下に迫ってくる）

【優先】
・敵キャラに耐久力を設定する（3発当たって死亡みたいな）
・大砲を置く（球が大砲に当たる→角度を調整して発射→敵死亡）
・自分が大砲になる？
TUI?

【可能であれば】
・マルチプレイヤー（大体４人位まで・協力して敵キャラ倒す）
・CLI実装（コマンドでパッドを操作）
・スマホ（Webブラウザ）で操作

【コマンド】
l 10
l -a 10
r 10
r -a 10

r 10













【ストーリー的な物】
・2114年、地球は危機に瀕していた。
　宇宙から来たブロック型生命体により侵略されようとしていた。
　
　そこで国際連合安全保障理事会は、反射型の新兵器を制作。
　遠隔で反射板を制御して、球を反射板と敵の間で反射させながらダメージを与える兵器である。

　つまりブロック崩しだよ！！！（投げやり）

　2114年5月14日、反射板のオペレーターに選ばれた貴方は、敵を月軌道を最終防衛ラインとし、迎え撃つ作戦に従事する。

　（なんだこの厨二的発想は）