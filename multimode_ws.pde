public class MultiMode_ws extends SingleMode 
{
    public int RandomNumber = 0;
    private JSONObject OccupyingBarData = new JSONObject();
    public MultiMode_ws () 
    {
        super();
        RandomNumber = int(random(1,1000));
    }
    public void Connecting()
    {   
        textAlign(CENTER);
        textSize(40);
        fill(255, 255, 255);
        text("CONNECTING", width * 0.5, height * 0.5);
        OccupyingBarData.setInt("Method_Status",23);
    }
    public void Setting(int[] barstatus) //ボールの初期位置を決める
	{
        /*
        OccupyingBarData.setInt("Left_Bar",1);
        OccupyingBarData.setInt("Left_Bar",1);
        OccupyingBarData.setInt("Left_Bar",1);
        OccupyingBarData.setInt("Left_Bar",1);
        */
		blocks.ShowBlocks();
		bars.ShowBar(barstatus);
		balls.ShowBall();
        balls.SetX_Coordinate(width / 2);
        balls.SetY_Coordinate(height / 2);
        textAlign(CENTER);
        textSize(28);
        if(frameCount % 120 > 60)
        {
            text("CHOOSE YOUR BAR!", width * 0.5, height * 0.1);
        }
	}
    public boolean Update(ControlStatus keyboardstatus, boolean IsKeyboardControl, boolean[] Controlling_Bar, int[] barstatus)//画面更新
    {
        if(!IsKeyboardControl)//マウス入力モード
        {
            for(int i = 0; i < 4; i++)
            {
                if(Controlling_Bar[i])
                {
                    bars.OnMouseMove(i);
                }
            }
        }
        else//キーボード入力モード
        {
            if(keyboardstatus.IsPressing_Up)
            {
                if(Controlling_Bar[2])
                {
                    bars.MoveToUp(2);
                }
                if(Controlling_Bar[3])
                {
                    bars.MoveToUp(3);
                }
            }
            if(keyboardstatus.IsPressing_Down)
            {
                if(Controlling_Bar[2])
                {
                    bars.MoveToDown(2);
                }
                if(Controlling_Bar[3])
                {
                    bars.MoveToDown(3);
                }
            }
            if(keyboardstatus.IsPressing_Left)
            {
                if(Controlling_Bar[0])
                {
                    bars.MoveToLeft(0);
                }
                if(Controlling_Bar[1])
                {
                    bars.MoveToLeft(1);
                }
            }
            if(keyboardstatus.IsPressing_Right)
            {
                if(Controlling_Bar[0])
                {
                    bars.MoveToRight(0);
                }
                if(Controlling_Bar[1])
                {
                    bars.MoveToRight(1);
                }
            }
        }

        blocks.ShowBlocks();//表示
		blocks.UpdateAngle();//回転させる
        bars.ShowBar(barstatus);//表示
        boolean isoutofcampus = balls.OnUpdatePosition();
        if(isoutofcampus)
        {
            return true;
        }
        balls.ShowBall();

        CheckCollision.CheckCollision_and_InvertBall(blocks,bars,balls);
        return false;
    }
   public void SetBarsPotision(int barNumber, float coordinate)
   {
       bars.SetBarsPotision(barNumber, coordinate);
   }
   public boolean CountDown(int[] barstatus)//カウントダウン
    {
        blocks.ShowBlocks();
		bars.ShowBar(barstatus);
		balls.ShowBall();
        passedframecount++;
        textAlign(CENTER);
        textSize(40);
        fill(255, 255, 255);
        if(passedframecount < 60)
        {
            text("3", width * 0.5, height * 0.5);
        }
        if(passedframecount > 60 && passedframecount <= 120)
        {
            text("2", width * 0.5, height * 0.5);
        }
        if(passedframecount > 120 && passedframecount <= 180)
        {
            text("1", width * 0.5, height * 0.5);
        }
        if(passedframecount > 180)//180フレーム=3秒（60fps*3）経ったら開始
        {
            passedframecount = 0;
            return true;
        }
        else
        {
            return false;    
        }
        
    }
    public void GameOver(int[] barstatus)//ゲームオーバー画面
    {
        blocks.ShowBlocks();
        bars.ShowBar(barstatus);

        fill(0, 102, 165);
        textAlign(CENTER);
        textSize(40);
        text("GAME OVER", width * 0.5, height * 0.08);
        if(frameCount % 120 > 60)
        {
            text("Click to Menu", width * 0.5, height * 0.95);
        }
    }
    public float GetCoordinate(int BarNumber)
    {
        if(BarNumber == 0 || BarNumber == 1)
        {
            return this.bars.GetX_Coordinate(BarNumber);
        }
        else
        {
            return this.bars.GetY_Coordinate(BarNumber);
        }
    }
}
