//ブロックです。BreakOutObjectを継承しています
public class BlockModel extends BreakOutObject implements IBreakOut
{
    private float PositionOfRadians = 0.0;
    private int Distance = 0;
    private boolean InverseRotate = false;
    private int BallInversedTime = 10;
    private boolean RecentInversed = false;
    protected int HP = 3 ;
    //コンストラクターです。BreakOutObjectのコンストラクターを呼び出しています
    public BlockModel (int x,int y,int width,int height)  
    {
        super(x,y,width,height);
        super.Y_PPF = 10;
    }
    //オーバーロード
    public BlockModel (float x,float y,int width,int height)  
    {
        super(x,y,width,height);
    }
    //オーバーロード
    public BlockModel (float x,float y,int width,int height,float radians,int distance,boolean inverse)  
    {
        super(x,y,width,height);
        this.PositionOfRadians = radians;
        this.Distance = distance;
        this.InverseRotate = inverse;
    }
    //ブロックを描画します
    public void Show()
    {
        stroke(0, 0, 0);
        switch(this.HP)
        {
            case 0:
                break;
            case 1:
                fill(255,0,0);
                rect(super.X_Coordinate, super.Y_Coordinate, super.Width, super.Height);
                //ellipse(super.X_Coordinate, super.Y_Coordinate, super.Width, super.Height);
                this.DrawCiecleForMagicSquare();
                break;
            case 2:
                fill(255,255,0);
                rect(super.X_Coordinate, super.Y_Coordinate, super.Width, super.Height);
                this.DrawCiecleForMagicSquare();
                break;
            case 3:
                fill(0,128,0);
                rect(super.X_Coordinate, super.Y_Coordinate, super.Width, super.Height);
                this.DrawCiecleForMagicSquare();
                break;
        }
    }
    private void DrawCiecleForMagicSquare()
    {
        stroke(0, 0, 255);
		noFill();
        ellipse(super.X_Coordinate + super.Width / 2, super.Y_Coordinate + super.Height / 2, super.Width * sqrt(2) , super.Height * sqrt(2));
    }
    
    public void ShowNumber(int number)
    {
        if(this.HP != 0)
        {
            text(number,super.X_Coordinate, super.Y_Coordinate);
        }
    }
    public void ShowHP()
    {
        if(this.HP != 0)
        {
            text(this.HP,super.X_Coordinate, super.Y_Coordinate);
        }
    }
        
    public float GetPositionOfRadians()
    {
        return this.PositionOfRadians;
    }
    public int GetDistance()
    {
        return this.Distance;
    }
    public boolean GetInverseRotate()
    {
        return this.InverseRotate;
    }
    public void UpdateInversedTime()
    {
        this.BallInversedTime = this.BallInversedTime != 10 && this.BallInversedTime != 0 
        ? this.BallInversedTime - 1
        : 10;
    }
    public void SetInversed()
    {
        //this.BallInversedTime = 9;
        this.RecentInversed = true;
    }
    public void ResetInversed(){this.RecentInversed = false;}
    public int GetInversedTime(){return this.BallInversedTime;}
    public boolean GetRecentInversed(){return this.RecentInversed;}
    public void DownHP(){this.HP -= 1;}
    public int GetHP(){return this.HP;}
}
public class BlockControler
{
    private BlockModel[] blocks = new BlockModel[21];
    private float Rotate_Radian = 0;
    FloatList CollidedXPoints = new FloatList();
    FloatList CollidedYPoints = new FloatList();
    public BlockControler ()    
    {
        float position_radian = -HALF_PI;
        int distance = 70;
        boolean inverse = false;
        for(int i = 0; i < this.blocks.length ; i++)
        {
           this.blocks[i] = new BlockModel(0,0,20,20,position_radian,distance,inverse);
           position_radian += i < 5 ? (1.0 / 5.0) * TWO_PI : HALF_PI / 2;
           if(i == 4)
           {
               position_radian = 0;
               distance = 150;
               inverse = true;
           }
           if(i == 12)
           {
               position_radian = 0;
               distance = 220;
               inverse = false;
           }
        }
        
    }
    public void ShowBlocks()
    {
        for (int i = 0; i < this.blocks.length; i++)
        {
            if(!this.blocks[i].GetInverseRotate())//回転させる処理
            {
                this.blocks[i].SetX_Coordinate(this.blocks[i].GetDistance() * cos(this.blocks[i].GetPositionOfRadians() + Rotate_Radian) + (width / 2)  - 10);
                this.blocks[i].SetY_Coordinate(this.blocks[i].GetDistance() * sin(this.blocks[i].GetPositionOfRadians() + Rotate_Radian) + (height / 2) - 10);
            }
            else//逆回転用
            {
                this.blocks[i].SetX_Coordinate(this.blocks[i].GetDistance() * cos(this.blocks[i].GetPositionOfRadians() + -Rotate_Radian) + (width / 2)  - 10);
                this.blocks[i].SetY_Coordinate(this.blocks[i].GetDistance() * sin(this.blocks[i].GetPositionOfRadians() + -Rotate_Radian) + (height / 2) - 10); 
            }
            
            this.blocks[i].Show();//表示
            this.blocks[i].UpdateInversedTime();//跳ね返った時に連続して何度も跳ね返らないようにする為のタイマーを更新している
        }
        this.DrawRectForMagicSquare();//背景の四角
        this.DrawStarForMagicSquare();//背景の星型
        this.DrawCircleForMagicSquare();//背景の円
    }
    public void UpdateAngle()
    {
        float angle = TWO_PI / 1024;
        this.Rotate_Radian = TWO_PI < Rotate_Radian + angle ? 0 : Rotate_Radian + angle;
    }
    private void DrawRectForMagicSquare()
    {
        stroke(0, 0, 255);
		noFill();
        for(int i = 5; i < 13; i++ )
        {
            if(i > 10 && this.blocks[i].GetHP() != 0 && this.blocks[i - 6].GetHP() != 0)
            {
                line(this.blocks[i].GetX_Coordinate() + this.blocks[i].GetWidth() / 2, this.blocks[i].GetY_Coordinate() + this.blocks[i].GetHeight() / 2, this.blocks[i - 6].GetX_Coordinate() + this.blocks[i - 6].GetWidth() / 2, this.blocks[i - 6].GetY_Coordinate() + this.blocks[i - 6].GetHeight() / 2);
            }
            else if(i <= 10 &&this.blocks[i].GetHP() != 0 && this.blocks[i + 2].GetHP() != 0)
            {
                line(this.blocks[i].GetX_Coordinate() + this.blocks[i].GetWidth() / 2, this.blocks[i].GetY_Coordinate() + this.blocks[i].GetHeight() / 2, this.blocks[i + 2].GetX_Coordinate() + this.blocks[i + 2].GetWidth() / 2, this.blocks[i + 2].GetY_Coordinate() + this.blocks[i + 2].GetHeight() / 2);
            }
        }
    }
    //024130の順に線を引き、星を描きます
    private void DrawStarForMagicSquare()
    {
        stroke(0, 0, 255);
		noFill();
        for(int i = 0; i < 4; i += 2)
        {
            if(this.blocks[i].GetHP() != 0 && this.blocks[i + 2].GetHP() != 0)
            {
                line(this.blocks[i].GetX_Coordinate() + this.blocks[i].GetWidth() / 2, this.blocks[i].GetY_Coordinate() + this.blocks[i].GetHeight() / 2, this.blocks[i + 2].GetX_Coordinate() + this.blocks[i + 2].GetWidth() / 2, this.blocks[i + 2].GetY_Coordinate() + this.blocks[i + 2].GetHeight() / 2);
            }
        }
        if(this.blocks[4].GetHP() != 0 && this.blocks[1].GetHP() != 0)
        {
            line(this.blocks[4].GetX_Coordinate() + this.blocks[4].GetWidth() / 2, this.blocks[4].GetY_Coordinate() + this.blocks[4].GetHeight() / 2, this.blocks[1].GetX_Coordinate() + this.blocks[1].GetWidth() / 2, this.blocks[1].GetY_Coordinate() + this.blocks[1].GetHeight() / 2);
        }
        for(int i = 1; i < 3; i += 2)
        {   
            if(this.blocks[i].GetHP() != 0 && this.blocks[i + 2].GetHP() != 0)
            {
                line(this.blocks[i].GetX_Coordinate() + this.blocks[i].GetWidth() / 2, this.blocks[i].GetY_Coordinate() + this.blocks[i].GetHeight() / 2, this.blocks[i + 2].GetX_Coordinate() + this.blocks[i + 2].GetWidth() / 2, this.blocks[i + 2].GetY_Coordinate() + this.blocks[i + 2].GetHeight() / 2);
            }
        }
        if(this.blocks[3].GetHP() != 0 && this.blocks[0].GetHP() != 0)
        {
            line(this.blocks[3].GetX_Coordinate() + this.blocks[3].GetWidth() / 2, this.blocks[3].GetY_Coordinate() + this.blocks[3].GetHeight() / 2, this.blocks[0].GetX_Coordinate() + this.blocks[0].GetWidth() / 2, this.blocks[0].GetY_Coordinate() + this.blocks[0].GetHeight() / 2);
        }
    }
    //一番外側の列の周りに円を描きます
    private void DrawCircleForMagicSquare()
    {
        for(int i = 13;i < this.blocks.length; i++)
        {
            if(this.blocks[i].GetHP() != 0)
            {
                stroke(0, 0, 255);
		        noFill();
                //28.28 = 20 * Route2
                ellipse(width / 2, height / 2, 440 + 28.28, 440 + 28.28);
		        ellipse(width / 2, height / 2, 440 - 28.28, 440 - 28.28);
                break;
            }
        }
    }
    //ブロックが円の時用。現在は使ってないので省略
    public FloatList CheckCollision_Circle(float ball_x,float ball_y,int ball_radius)
    {
        for(int i = 0; i < this.blocks.length; i++)
        {
            //if(this.blocks[i].GetInversedTime() != 10){continue;}
            if(this.blocks[i].GetHP() == 0){continue;}

            float block_center_X = this.blocks[i].GetX_Coordinate();
            float block_center_Y = this.blocks[i].GetY_Coordinate();
            int block_radius = this.blocks[i].GetWidth() / 2;
            if(sqrt((ball_x - block_center_X) * (ball_x - block_center_X) + (ball_y - block_center_Y) * (ball_y - block_center_Y)) < ball_radius + block_radius)
            {
                println("COLLISION");
                float radian = atan2(ball_y - block_center_Y,ball_x - block_center_X);
                radian = radian < 0 ? TWO_PI + radian : radian ;
                //左上から左上への反転命令
                if(CheckCollision.CheckBetWeen(3 * PI / 2, PI, radian))
                {
                    this.blocks[i].SetInversed();
                    this.blocks[i].DownHP();
                    return new FloatList(4,block_center_X + cos(radian) * (block_radius + ball_radius), block_center_Y + sin(radian) * (block_radius + ball_radius));
                }
                //左下から左下への反転命令
                if(CheckCollision.CheckBetWeen(PI, HALF_PI,radian))
                {
                    this.blocks[i].SetInversed();
                    this.blocks[i].DownHP();
                    return new FloatList(5,block_center_X + cos(radian) * (block_radius + ball_radius), block_center_Y + sin(radian) * (block_radius + ball_radius));
                }
                //右上から右上への反転命令
                if(CheckCollision.CheckBetWeen(TWO_PI,3 * PI / 2,radian))
                {
                    this.blocks[i].SetInversed();
                    this.blocks[i].DownHP();
                    return new FloatList(6,block_center_X + cos(radian) * (block_radius + ball_radius), block_center_Y + sin(radian) * (block_radius + ball_radius));
                }
                //右下から右下への反転命令
                if(CheckCollision.CheckBetWeen(HALF_PI, 0,radian))
                {
                    this.blocks[i].SetInversed();   
                    this.blocks[i].DownHP();
                    return new FloatList(7,block_center_X + cos(radian) * (block_radius + ball_radius), block_center_Y + sin(radian) * (block_radius + ball_radius));
                }
            }
        }
        return new FloatList(21,0);
    }
    public FloatList CheckCollision(float ball_x,float ball_y,int radius)
    {
        float ball_right_X = ball_x + radius;
        float ball_left_X = ball_x - radius;
        float ball_up_X = ball_x;
        float ball_down_X = ball_x;

        float ball_right_Y = ball_y;
        float ball_left_Y = ball_y;
        float ball_up_Y = ball_y - radius;
        float ball_down_Y = ball_y + radius;

        for(int i = 0; i < this.blocks.length ; i++)
        {
            //if(this.blocks[i].GetInversedTime() != 10){continue;}
            if(this.blocks[i].GetHP() == 0){continue;}
            float block_up_Y = this.blocks[i].GetY_Coordinate();
            float block_down_Y = this.blocks[i].GetY_Coordinate() + this.blocks[i].GetHeight();
            float block_left_X = this.blocks[i].GetX_Coordinate();
            float block_right_X = this.blocks[i].GetX_Coordinate() + this.blocks[i].GetWidth();
            
            float block_center_X = (block_left_X + block_right_X) / 2;
            float block_center_Y = (block_up_Y + block_down_Y) / 2;
            
            float inversed = this.blocks[i].GetRecentInversed() ? 1 : 0;
            if(i > -100)
            {
                //左上から左上への反転命令
                if(sqrt((ball_x - block_left_X) * (ball_x - block_left_X) + (ball_y - block_up_Y) * (ball_y - block_up_Y) ) < radius )
                {
                    ResetInversed();
                    this.blocks[i].SetInversed();
                    if(inversed == 0){this.blocks[i].DownHP();}
                    return new FloatList(4,block_up_Y - 4,inversed);
                }
                //左下から左下への反転命令
                if(sqrt((ball_x - block_left_X) * (ball_x - block_left_X) + (ball_y - block_down_Y) * (ball_y - block_down_Y) ) < radius)
                {
                    ResetInversed();
                    this.blocks[i].SetInversed();
                    if(inversed == 0){this.blocks[i].DownHP();}
                    return new FloatList(5,block_down_Y + 4,inversed);
                }
                //右上から右上への反転命令
                if(sqrt((ball_x - block_right_X) * (ball_x - block_right_X) + (ball_y - block_up_Y) * (ball_y - block_up_Y) ) < radius)
                {
                    ResetInversed();
                    this.blocks[i].SetInversed();
                    if(inversed == 0){this.blocks[i].DownHP();}
                    return new FloatList(6,block_up_Y - 4,inversed);
                }
                //右下から右下への反転命令
                if(sqrt((ball_x - block_right_X) * (ball_x - block_right_X) + (ball_y - block_down_Y) * (ball_y - block_down_Y) ) < radius)
                {
                    ResetInversed();

                    this.blocks[i].SetInversed();
                    if(inversed == 0){this.blocks[i].DownHP();}
                    return new FloatList(7,block_down_Y + 4,inversed);
                }
            }
            //左から右への反転命令
            float radian = atan2(ball_left_Y - block_center_Y,ball_left_X - block_center_X);
            radian = radian < 0 ? TWO_PI + radian : radian ;
            if(CheckCollision.CheckBetWeen(7 + PI / 4, TWO_PI, radian) || CheckCollision.CheckBetWeen(0, PI / 4, radian))
            {
                if(CheckCollision.CheckBetWeen(block_up_Y,block_down_Y,ball_left_Y) && CheckCollision.CheckBetWeen(block_left_X,block_right_X,ball_left_X))
                {
                    ResetInversed();

                    this.blocks[i].SetInversed();
                    CollidedXPoints.append(ball_left_X + 4);
                    CollidedYPoints.append(ball_left_Y);
                    if(inversed == 0){this.blocks[i].DownHP();}
                    return new FloatList(2,block_right_X + radius,inversed);
                }
            }
            //右から左への反転命令
            radian = atan2(ball_right_Y - block_center_Y,ball_right_X - block_center_X);
            radian = radian < 0 ? TWO_PI + radian : radian ;
            if(CheckCollision.CheckBetWeen(3 * PI / 4, 5 * PI / 4, radian));
            {
                if(CheckCollision.CheckBetWeen(block_up_Y,block_down_Y,ball_right_Y) && CheckCollision.CheckBetWeen(block_left_X,block_right_X,ball_right_X))
                {
                    ResetInversed();

                    this.blocks[i].SetInversed();
                    CollidedXPoints.append(ball_right_X + 8);
                    CollidedYPoints.append(ball_right_Y);
                    if(inversed == 0){this.blocks[i].DownHP();}
                    return new FloatList(3,block_left_X - radius,inversed);
                }
            }
            //上から下への反転命令
            radian = atan2(ball_up_Y - block_center_Y,ball_up_X - block_center_X);
            radian = radian < 0 ? TWO_PI + radian : radian ;
            if(CheckCollision.CheckBetWeen(PI / 4,3 * PI / 4,radian))
            {
                if(CheckCollision.CheckBetWeen(block_up_Y,block_down_Y,ball_up_Y) && CheckCollision.CheckBetWeen(block_left_X,block_right_X,ball_up_X))
                {
                    ResetInversed();

                    this.blocks[i].SetInversed();
                    CollidedXPoints.append(ball_up_X + 4);
                    CollidedYPoints.append(ball_up_Y - 8);
                    if(inversed == 0){this.blocks[i].DownHP();}
                    return new FloatList(0,block_down_Y + radius,inversed);
                }
            }

            //下から上への反転命令
            radian = atan2(ball_down_Y - block_center_Y,ball_down_X - block_center_X);
            radian = radian < 0 ? TWO_PI + radian : radian ;
            if(CheckCollision.CheckBetWeen(5 * PI / 4,7 * PI / 4,radian))
            {
                if(CheckCollision.CheckBetWeen(block_up_Y,block_down_Y,ball_down_Y) && CheckCollision.CheckBetWeen(block_left_X,block_right_X,ball_down_X))
                {
                    ResetInversed();

                    this.blocks[i].SetInversed();   
                    CollidedXPoints.append(ball_down_X);
                    CollidedYPoints.append(ball_down_Y - 8);
                    if(inversed == 0){this.blocks[i].DownHP();}
                    return new FloatList(1,block_up_Y - radius,inversed);
                }
            }
            
        }
        return new FloatList(21,1);//何も無かったら21
        
    }
    public void ResetInversed()
    {
        for(int i = 0; i < this.blocks.length; i++)
        {
            this.blocks[i].ResetInversed(); 
        }
        println("RESET");
    }
}
