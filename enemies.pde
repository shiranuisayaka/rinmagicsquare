//敵です。BreakOutObjectを継承してほぼそのまま使っています
public class EnemyModel extends BreakOutObject implements IBreakOut
{
    //PPF = Pixel Per Frame.1フレームに何ピクセル動くか
    private int X_PPF = 0;
    private int Y_PPF = 1;

    //コンストラクターです。BreakOutObjectのコンストラクターを呼び出しているだけです
    public EnemyModel (int x,int y,int width,int height)  
    {
        super(x,y,width,height);
    }
    public void UpdateXCoordinate()
    {
        super.X_Coordinate += X_PPF;
    }
    public void UpdateXCoordinate(int x)
    {
        super.X_Coordinate = x;
    }
    public void UpdateYCoordinate()
    {
        super.Y_Coordinate += Y_PPF;
    }
    public void UpdateYCoordinate(int y)
    {
        super.Y_Coordinate = y;
    }
    public void Show()
    {
        rect(super.X_Coordinate, super.Y_Coordinate, super.Width, super.Height);
    }
}

public class EnemyControler
{
    private ArrayList<EnemyModel> enemies = new ArrayList<EnemyModel>();
    private int TargetRandomNumber = 0;
    public EnemyControler ()    
    {
        TargetRandomNumber = int(random(200));
    }
    public void ShowEnemies()
    {
        if(!enemies.isEmpty())
        {
            for (int i = 0; i < enemies.size(); i++) 
            {
                enemies.get(i).UpdateYCoordinate();
                if(enemies.get(i).GetY_Coordinate() > height )
                {
                    enemies.remove(i);
                }
                else
                {
                    enemies.get(i).Show();    
                }
            }
            println(enemies.size());
        }
    }
    public void EnemyCapsuletoys()
    {
        if(int(random(200)) == this.TargetRandomNumber)
        {
            this.enemies.add(new EnemyModel(int(random(325)),0,20,20));
        }
    }
}
