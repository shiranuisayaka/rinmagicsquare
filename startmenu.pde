public class StartMenu
{
    //スタートメニューを表示させてる
    public void ShowStartMenu(boolean IsKeyboardControl, boolean IsSmartPhoneControl) 
    {
        stroke(0, 0, 0);
        fill(0, 102, 165);
        textAlign(CENTER);
        textSize(40);
        text("RiN's Magic Square", width * 0.5, height * 0.1);
        fill(255, 255, 255);
        textSize(34);
        
        text("DESTROY She's Fantasy!", width * 0.5, height * 0.3);
        
        stroke(0, 102, 165);
        
        if(mouseX > width * 0.25 && mouseX < width * 0.75
        && mouseY > height * 0.5 && mouseY < height * 0.6)
        {
			fill(0, 102, 165);
        }
        else
        {
			fill(0, 0, 0);
        }
        rect(width * 0.25, height * 0.5, width * 0.5, height * 0.1);
        fill(255, 255, 255);
		stroke(0, 0, 0);
		text("SINGLE PLAYER", width * 0.5, height * 0.57);

        stroke(0, 102, 165);
		if(mouseX > width * 0.25 && mouseX < width * 0.75
        && mouseY > height * 0.7 && mouseY < height * 0.8)
        {
			fill(0, 102, 165);
        }
		else
		{
			fill(0, 0, 0);
		}
		rect(width * 0.25, height * 0.7, width * 0.5, height * 0.1);
        fill(255, 255, 255);
        stroke(0, 0, 0);
        text("MULTI PLAYER", width * 0.5, height * 0.77);

        textSize(16);
        textAlign(LEFT);
		noFill();
		stroke(0, 102, 165);
		rect(width * 0.03, height * 0.85, width * 0.03, height * 0.03);
        //マウス操作ボックス
        if(!IsKeyboardControl && !IsSmartPhoneControl)
        {
		    ellipse(width * 0.045, height * 0.865, width * 0.02, height * 0.02);
        }
        text("MOUSE", width * 0.07, height * 0.875);
		rect(width * 0.03, height * 0.89, width * 0.03, height * 0.03);
        //キーボード操作ボックス
        if(IsKeyboardControl && !IsSmartPhoneControl)
        {
		    ellipse(width * 0.045, height * 0.905, width * 0.02, height * 0.02);
        }
        text("KEYBOARD", width * 0.07, height * 0.915);
		rect(width * 0.03, height * 0.93, width * 0.03, height * 0.03);
        //スマホ操作ボックス
        if(IsSmartPhoneControl)
        {
		    ellipse(width * 0.045, height * 0.945, width * 0.02, height * 0.02);
        }
        text("SMARTPHONE", width * 0.07, height * 0.955);
		
        stroke(0, 0, 0);
        textAlign(RIGHT);
        textSize(12);
        text("Created by Shiranui Nui v3.0 β", width , height * 0.99);
    }

}


